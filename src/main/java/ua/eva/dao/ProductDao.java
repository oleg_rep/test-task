package ua.eva.dao;

import ua.eva.model.Product;

import java.util.List;

public interface ProductDao {

    List<Product> getProduct();
}
