package ua.eva.dao.mapper;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ua.eva.model.Product;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ProductRowMapper implements RowMapper<Product> {

    @Override
    public Product mapRow(ResultSet row, int i) throws SQLException {

        Product product = new Product();

        product.setId(row.getInt("id"));
        product.setName(row.getString("name"));
        product.setDescription(row.getString("description"));

        return product;
    }
}
