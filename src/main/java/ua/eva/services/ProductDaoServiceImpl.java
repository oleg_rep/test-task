package ua.eva.services;

import org.springframework.stereotype.Service;
import ua.eva.dao.ProductDao;
import ua.eva.model.Product;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ProductDaoServiceImpl implements ProductDaoService {

    private ProductDao productDao;
    private List<Product> products;

//    public ProductDaoServiceImpl() {}


    public ProductDaoServiceImpl(ProductDao productDao) {
        this.productDao = productDao;
    }

    private List<Product> getFilteredProducts(String regEx, List<Product> products) {
        Collections.sort(products, (Product p1, Product p2) -> p1.getName().compareTo(p2.getName()));
        List<Product> productsSearch = new ArrayList<>();
        Pattern regexp = Pattern.compile(regEx);

        // Фильтр для вывода всех записей
        if (regEx.equals("^*.*$")) {
            productsSearch.addAll(products);
        } else {
            // Фильтр RegExp
            for (Product p : products) {
                Matcher m = regexp.matcher(p.getName());
                if (!m.find()) {
                    productsSearch.add(p);
                }
            }
        }
        return productsSearch;
    }

    //Поиск по коллекции
    @Override
    public List<Product> findProduct(String regEx) {
        products = productDao.getProduct();
        return getFilteredProducts(regEx, products);
    }

    @Override
    public List<Product> findProduct(String regEx, List<Product> products) {
        return getFilteredProducts(regEx, products);
    }
}
