package ua.eva.services;

import ua.eva.model.Product;

import java.util.List;

public interface ProductDaoService {

    List<Product> findProduct(String regEx);

    List<Product> findProduct(String regEx, List<Product> products);
}
