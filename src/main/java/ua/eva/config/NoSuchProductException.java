package ua.eva.config;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "There are no such products")
public class NoSuchProductException extends RuntimeException {
}


