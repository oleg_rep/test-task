package ua.eva.controller;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ua.eva.config.NoSuchProductException;
import ua.eva.model.Product;
import ua.eva.services.ProductDaoService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@RestController
public class MainController {

    private ProductDaoService productDaoService;
    Logger log = Logger.getLogger(MainController.class.getName());

    public MainController(ProductDaoService productDaoService) {
        this.productDaoService = productDaoService;
    }

    @GetMapping(value = "/shop/product")
    public List<Product> getIndex(@RequestParam("paramName") String paramName) {
        List<Product> products = productDaoService.findProduct(paramName);

        if (products.isEmpty()) {
            log.error("There are no such products (Error 404)");
            throw new NoSuchProductException();
        } else {
            log.info("for filter " + paramName + " found " + products.size() + " products" );
            return products;
        }
    }
}
