
--create database shop;

drop  table  products;

create table products(id serial not null primary key,
                      name varchar(100) not null ,
                      description varchar(1000)  not null );



insert into products(name, description)
values ('Pizza','Very tasty pizzza'),
       ('Шампунь' , 'Шолковистые волосы'),
       ('Шампунь' , 'Пышные волосы'),
       ('Армани' , 'Дейзик'),
       ('Арлуни' , 'Дейзик'),
       ('Мыло' , 'Мыло'),
       ('Бували' , 'Туалетная вода');

select * from products;