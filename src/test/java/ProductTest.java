import org.junit.Test;
import ua.eva.model.Product;
import ua.eva.services.ProductDaoService;
import ua.eva.services.ProductDaoServiceImpl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class ProductTest {

    private List<Product> products;

    {
        products = new ArrayList<>();
        products.add(new Product(1, "eva plus", "Духи"));
        products.add(new Product(2, "Шампунь", "Шолковистые волосы"));
        products.add(new Product(3, "Шампунь", "Пышные волосы"));
        products.add(new Product(4, "Армани", "Дейзик"));
        products.add(new Product(5, "Арлуни", "Дейзик"));
        products.add(new Product(6, "Мыло", "Мыло"));
        products.add(new Product(7, "Бували", "Туалетная вода"));
    }

    @Test
    public void getAllProductsTest() {
        ProductDaoService productDaoService = new ProductDaoServiceImpl(null);
        List<Product> productList = productDaoService.findProduct("^*.*$", products);
        int expected = 7;
        assertEquals(expected, productList.size());
    }

    @Test
    public void getFirstLetProductsTest() throws SQLException {
        ProductDaoService productDaoService = new ProductDaoServiceImpl(null);
        List<Product> productList = productDaoService.findProduct("^Ш.*$", products);
        int expected = 5;
        assertEquals(expected, productList.size());
    }

    @Test
    public void getMuchtLetProductsTest() throws SQLException {
        ProductDaoService productDaoService = new ProductDaoServiceImpl(null);
        List<Product> productList = productDaoService.findProduct("^*.[eva].*$", products);
        int expected = 6;
        assertEquals(expected, productList.size());
    }
}